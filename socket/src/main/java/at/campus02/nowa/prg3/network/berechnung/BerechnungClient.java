package at.campus02.nowa.prg3.network.berechnung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class BerechnungClient {

	public static void main(String[] args) throws Exception {
		Socket client = new Socket("localhost", 1113);
		BufferedReader brc = new BufferedReader(new InputStreamReader(System.in));
		ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
		ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());
		oos.flush();
		String line;
		while ((line = brc.readLine()) != null) {
			String[] fields = line.split(" ");
			OperationEnum o;
			switch (fields[1]) {
			case "+":
				o = OperationEnum.ADDITION;
				break;
			case "-":
				o = OperationEnum.SUBTRAKTION;
				break;
			case "*":
				o = OperationEnum.MULTIPLIKATION;
				break;
			case "/":
				o = OperationEnum.DIVISION;
				break;
			case "%":
				o = OperationEnum.MODULO;
				break;
			default:
				throw new Exception("Üngültige Operation");
			}
			Berechnung ber = new Berechnung(Double.valueOf(fields[0]), Double.valueOf(fields[2]), o);
			oos.writeObject(ber);
			oos.flush();
			Ergebnis antwort = (Ergebnis) ois.readObject();
			if (antwort.isErfolg()) {
				System.out.println("Das Ergebnis lautet: " + antwort.getE());
			} else {
				System.out.println("Berechnung fehlgeschlagen!");
			}
		}
		client.close();
	}

}
