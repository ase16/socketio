package at.campus02.nowa.prg3.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class ObjectPingPongClient {

	public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {
		Socket client = new Socket("localhost", 1112);
		BufferedReader brc = new BufferedReader(new InputStreamReader(System.in));
		ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
		ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());
		oos.flush();
		String line;
		while ((line = brc.readLine()) != null) {
			oos.writeObject(line);
			oos.flush();
			String antwort = (String) ois.readObject();
			System.out.println(antwort);
		}
		client.close();
	}

}
