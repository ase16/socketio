package at.campus02.nowa.prg3.network.berechnung;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class BerechnungServer {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		ServerSocket server = new ServerSocket(1113);
		while (true) {
			Socket connection = server.accept();
			ObjectOutputStream oos = new ObjectOutputStream(connection.getOutputStream());
			oos.flush();
			ObjectInputStream ois = new ObjectInputStream(connection.getInputStream());
			Berechnung ber;
			try {
				while ((ber = (Berechnung) ois.readObject()) != null) {
					switch (ber.getO()) {
					case ADDITION:
						oos.writeObject(new Ergebnis(ber.getA() + ber.getB(), true));
						break;
					case SUBTRAKTION:
						oos.writeObject(new Ergebnis(ber.getA() - ber.getB(), true));
						break;
					case MULTIPLIKATION:
						oos.writeObject(new Ergebnis(ber.getA() * ber.getB(), true));
						break;
					case DIVISION:
						if (ber.getB() == 0) {
							oos.writeObject(new Ergebnis(0, false));
						} else {
							oos.writeObject(new Ergebnis(ber.getA() / ber.getB(), true));
						}
						break;
					case MODULO:
						oos.writeObject(new Ergebnis(ber.getA() % ber.getB(), true));
						break;
					default:
						oos.writeObject(new Ergebnis(0, false));
						break;
					}
					oos.flush();
				}
			} catch (EOFException e) {
				System.out.println("Kein Object mehr vom Client empfangen!");
			} catch (SocketException e) {
				System.out.println("Verbindung wurde getrennt!");
			}
			connection.close();
		}

	}

}
