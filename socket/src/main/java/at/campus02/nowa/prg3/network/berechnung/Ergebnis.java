package at.campus02.nowa.prg3.network.berechnung;

import java.io.Serializable;

public class Ergebnis implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8541010727084250773L;
	private final double e;
	private final boolean erfolg;
	
	public Ergebnis(double e, boolean erfolg) {
		super();
		this.e = e;
		this.erfolg = erfolg;
	}

	public double getE() {
		return e;
	}

	public boolean isErfolg() {
		return erfolg;
	}

}
