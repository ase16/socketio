package at.campus02.nowa.prg3.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoSocketServerDemo {

	public static void main(String[] args) throws IOException {
		ServerSocket server = new ServerSocket(9090);
		while (true) {
			Socket connection = server.accept();
			InputStream is = connection.getInputStream();
			OutputStream os = connection.getOutputStream();
			int byteRead;
			while ((byteRead = is.read()) != -1) {
				System.out.println("Server leitet Byte weiter: " + byteRead);
				os.write(byteRead + 1);
			}
		}
	}

}
