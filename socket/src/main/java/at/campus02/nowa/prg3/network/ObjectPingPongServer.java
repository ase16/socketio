package at.campus02.nowa.prg3.network;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class ObjectPingPongServer {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		ServerSocket server = new ServerSocket(1112);
		while (true) {
			Socket connection = server.accept();
			ObjectOutputStream oos = new ObjectOutputStream(connection.getOutputStream());
			oos.flush();
			ObjectInputStream ois = new ObjectInputStream(connection.getInputStream());
			String line;
			try {
			while ((line = (String) ois.readObject()) != null) {
				if (line.toLowerCase().equals("ping")) {
					oos.writeObject("pong vom server");
				} else if (line.toLowerCase().equals("pong")) {
					oos.writeObject("ping vom server");
				} else {
					oos.writeObject("falsch");
				}
				oos.flush();
			}
			} catch (EOFException e) {
				System.out.println("Kein Object mehr vom Client empfangen!");
			} catch (SocketException e) {
				System.out.println("Verbindung wurde getrennt!");
			}
			connection.close();
		}

	}

}
