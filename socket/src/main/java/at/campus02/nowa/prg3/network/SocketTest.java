package at.campus02.nowa.prg3.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketTest {

	public static void main(String[] args) throws UnknownHostException, IOException {
		Socket client = new Socket("hetzner.fladi.at", 7);
		OutputStream os = client.getOutputStream();
		InputStream is = client.getInputStream();
		PrintWriter pw = new PrintWriter(new OutputStreamWriter(os));
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		pw.println("Hallo Michael!");
		System.out.println(br.readLine());
		pw.close();
		client.close();
	}

}
