package at.campus02.nowa.prg3.network.berechnung;

import java.io.Serializable;

public class Berechnung implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5872101270787893632L;
	private final double a;
	private final double b;
	private final OperationEnum o;

	public Berechnung(double a, double b, OperationEnum o) {
		super();
		this.a = a;
		this.b = b;
		this.o = o;
	}

	public double getA() {
		return a;
	}

	public double getB() {
		return b;
	}

	public OperationEnum getO() {
		return o;
	}

}
