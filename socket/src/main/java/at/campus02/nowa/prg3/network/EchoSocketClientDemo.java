package at.campus02.nowa.prg3.network;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class EchoSocketClientDemo {

	public static void main(String[] args) throws UnknownHostException, IOException {
		Socket client = new Socket("hetzner.fladi.at", 9090);
		OutputStream os = client.getOutputStream();
		InputStream is = client.getInputStream();
		for (int i = 1; i<256; i++) {
			os.write(i);
			os.flush();
			int received = is.read();
			System.out.println("Client empfängt Byte: " + received);	
		}
		client.close();

	}

}
