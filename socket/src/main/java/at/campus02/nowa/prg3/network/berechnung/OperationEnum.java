package at.campus02.nowa.prg3.network.berechnung;

public enum OperationEnum {
	
	ADDITION,
	SUBTRAKTION,
	MULTIPLIKATION,
	DIVISION,
	MODULO

}
