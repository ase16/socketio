package at.campus02.nowa.prg3.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class PingPongClient {

	public static void main(String[] args) throws UnknownHostException, IOException {
		Socket client = new Socket("localhost", 1111);
		BufferedReader brc = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter pw = new PrintWriter(client.getOutputStream());
		BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
		String line;
		while ((line = brc.readLine()) != null) {
			pw.println(line);
			pw.flush();
			String antwort = br.readLine();
			System.out.println(antwort);
		}
		client.close();
	}

}
