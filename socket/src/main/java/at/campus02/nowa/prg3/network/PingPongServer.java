package at.campus02.nowa.prg3.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class PingPongServer {

	public static void main(String[] args) throws IOException {
		ServerSocket server = new ServerSocket(1111);
		while (true) {
			Socket connection = server.accept();
			InputStream is = connection.getInputStream();
			OutputStream os = connection.getOutputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			PrintWriter pw = new PrintWriter(os);
			String line;
			while ((line = br.readLine()) != null) {
				if (line.toLowerCase().equals("ping")) {
					pw.println("pong vom server");
				} else if (line.toLowerCase().equals("pong")) {
					pw.println("ping vom server");
				} else {
					pw.println("falsch");
				}
				pw.flush();
			}
			connection.close();
		}

	}

}
