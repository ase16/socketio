package at.campus02.nowa.prg3.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class WebClientTest {

	public static void main(String[] args) throws UnknownHostException, IOException {
		String request = "GET /DE/Homepage.aspx HTTP/1.1\r\nHost: www.campus02.at\r\n\r\n";
		Socket client = new Socket("www.campus02.at", 80);
		OutputStream os = client.getOutputStream();
		InputStream is = client.getInputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os);
		osw.write(request);
		osw.flush();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		client.close();
		
		
	}

}
