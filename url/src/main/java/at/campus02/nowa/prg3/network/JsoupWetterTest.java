package at.campus02.nowa.prg3.network;

import java.io.IOException;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class JsoupWetterTest {

	public static void main(String[] args) throws IOException {
		Connection conn = Jsoup.connect("http://wetter.orf.at/steiermark/prognose");
		Document doc = conn.get();
		System.out.println(doc.title());
		Elements elements = doc.select("#content .storyWrapper #ss-storyText .fulltextWrapper > h2");
		for (Element e : elements) {
			System.out.println(e.text() + ":");
			System.out.println(((Element) e.nextSibling().nextSibling()).text() + "\n");
		}
				
	}

}
