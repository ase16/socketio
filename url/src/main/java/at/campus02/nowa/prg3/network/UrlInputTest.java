package at.campus02.nowa.prg3.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class UrlInputTest {

	public static void main(String[] args) {
		try (
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
		) {
			String line;
			while ((line = br.readLine()) != null) {
				URL url = new URL(line);
				try (
					BufferedReader bru = new BufferedReader(new InputStreamReader(url.openStream()));
				) {
					String lineu;
					while ((lineu = bru.readLine()) != null) {
						System.out.println(lineu);
					}
				} catch (IOException e) {
					System.out.println("Fehler in der Netzwerkkommunikation!");
				}
			}
			
		} catch (IOException e) {
			System.out.println("Fehler beim Lesen von Konsole!");
		}

	}

}
