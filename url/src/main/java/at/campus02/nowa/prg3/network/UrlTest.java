package at.campus02.nowa.prg3.network;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;

public class UrlTest {

	public static void main(String[] args) {
		File file = new File("wetter.html");
		try {
			URL url = new URL("http://wetter.orf.at/steiermark/prognose");
			try (
				BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
				PrintWriter pw = new PrintWriter(new FileWriter(file));
			) {
				String line;
				while ((line = br.readLine()) != null) {
					System.out.println(line);
					pw.println(line);
				}
			} catch (IOException e) {
				System.out.println("Konnte nicht von URL lesen!");
			}
			
		} catch (MalformedURLException e) {
			System.out.println("Keine gültige URL!");
			return;
		}

	}

}
